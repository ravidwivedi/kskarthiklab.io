/*
  * Name: Crypto Sim
  * Author: Sai Karthik [kskarthik at disroot dot org]
  * License:GPLv3
*/
'use strict'

// Initializes the user's balance value
let usr = {

    "amt": document.querySelector("#userAmt"),
    "bal": localStorage.getItem('USER_BAL'),
    "tcount": localStorage.getItem('T_COUNT')
   }
var json; 
 // parses API from koinex.in
function loadPrices() {

const url = "https://koinex.in/api/ticker"

 fetch(url, {mode: "cors"}).then(function(response) {
    
    return response.json();
    
  }).then(function(myJson) {
  
  JSON.stringify(myJson);
  
  json = myJson.prices.inr;
  });
}

function usrinit() {
  
  const USER_INIT_BAL = 500000;
    
   if (localStorage.length == 0) {
   
    localStorage.setItem('USER_BAL', USER_INIT_BAL); // create user inr wallet
     localStorage.setItem('T_COUNT', 1); // create a counter for transaction    
      setTimeout(genWallets, 1000);
  
   } 
  } 
  
  usrinit();

  // Generate user wallets
  function genWallets() {
    
    if (localStorage.length == 2) {
  
     console.log('creating user wallets');
     
   let y;
    
     for (y in json) {
       
        localStorage.setItem(y, 0);
      
    }
    location.reload();
   }
  }

// Display User wallets in portfolio 
function usrWallets() {
 
    console.log('Showing user wallets');

    usr.amt.textContent = parseFloat(usr.bal).toFixed(2);

     if (document.querySelector('#loader')) {
     
      document.querySelector('#loader').remove();

    }

     if (document.querySelector('#wallets').childNodes.length == 0) {
      
     let v, wallets, span, select, option; 
      // list user wallets 
      for (v in json) {
    
      if ( (localStorage.getItem(v) != 0) &&(localStorage.getItem(v) !=null) ) {
        wallets = document.getElementById('wallets');  
          span = document.createElement('p');
           span.id = v;
             span.innerHTML = "<b class= 'animated rollIn'>" + v +"</b>" + ": " + localStorage.getItem(v);
               wallets.appendChild(span);
      
       } 
       // load buy/sell currency options 
       select = document.querySelector('#cointype');
         option = document.createElement('option');
          option.textContent = v;
           select.appendChild(option);

        // Create new wallets if a new coin is added to the api
           if (localStorage.getItem(v) == null) {
                    
             localStorage.setItem(v, 0);
           }
       } // end of loop
     }
    
     if (wallets.childNodes.length == 0) {
    
      wallets.innerHTML = "<br><div class='alert alert-warning' role=alert><i>Your wallets are empty </i>🙄</div>"
     } 
    }


/*************************************************************
 
 Transaction Area
 
**************************************************************/

// returns parsed values as int & float
function pi(v) { 

  return parseInt(v); 
}

function pf(v) { 

  return parseFloat( parseFloat(v).toFixed(2) );
}

let t = { 

 "select": document.querySelector('#transact'),
 "transact": null,
 "coin": null,
 "qty": null,
 "done": document.querySelector('#done'),

}

// update values when necessary  
function updateValues() {
 
 t.transact = t.select.value;
 t.coin = document.querySelector('#cointype').value
 t.qty = document.querySelector('#qty').value;

}

let stat = document.querySelector('#status');

// check for user selected transaction type & perform task accordingly
t.done.onclick = check;

function check() {

 updateValues();

 if ((t.transact == "Buy") && ( (t.qty %1) == 0) && (t.qty !="")) {
 
  Buy(t.coin, t.qty);
 
 } else {
    
  if ((t.transact == "Sell") && ( (t.qty%1) == 0) && (t.qty !="")) {
 
    Sell(t.coin, t.qty);
  
  } else {

    swal("Please choose quantity 1 or above", {icon:"info"});

  } 
 }
}

// This function is called when user buys coins
function Buy(c, q) {
  
 let isum, sum, fees, x;
   
  for (x in json) {

   isum = ( pf(json[x]) * pi(q) );

   fees = pf( (isum * 0.15)/100 );

   //Set minimum transaction fees
   if ( pf(fees) < 0.02) {

    fees = 0.02;

   }
    
   sum = pf(isum) + pf(fees);

   if ( (c == x) && (pf(usr.bal) >= sum ) ) {

    let count = pi(usr.tcount) + 1;
      localStorage.setItem('T_COUNT', count);
       localStorage.setItem(x, pi(localStorage.getItem(x)) + pi(q) );
        localStorage.setItem('USER_BAL', pf(usr.bal) - sum);

       // Save the transaction to localStorage
      let tnx = {
         
        "type": "BUY",
        "coin": c,
        "qty": q,
        "val": json[x],
        "date": Date().slice(4, 21)
                
      };

      localStorage.setItem( "USR_TXN" + usr.tcount, JSON.stringify(tnx) );

       swal("Debited : ₹" + pf(sum) +  "\n\nQuantity : " + q + "\n\nFees (0.15%) : ₹" + fees, {title: c + " Transaction Successful !", icon:"success"})
       .then((value => { location.reload() } ));
  }
 }
}

// this function is called when user wants to sell
function Sell(c, q) {

let isum, sum, fees, x;   
 for (x in json) {

   isum = ( pf(json[x]) * pi(q) );

   fees = pf( (isum * 0.15)/100 );

   //Set minimum transaction fees
   if (fees < 0.02) {

    fees = 0.02;

   }

   sum = isum - fees; 

    if ( (c == x) && (pi(localStorage.getItem(x)) >= q) ) { // if user's wallet quantity is == sell quantity
      
      let count = pi(usr.tcount) + 1;
      localStorage.setItem('T_COUNT', count);
       localStorage.setItem(x, pi(localStorage.getItem(x)) - pi(q) );
        localStorage.setItem('USER_BAL', pf(usr.bal) + sum);

       // Save the transaction to localStorage
      let tnx = {
         
        "type": "SELL",
        "coin": c,
        "qty": q,
        "val": json[x],
        "date": Date().slice(4, 21)
                
      };

      localStorage.setItem( "USR_TXN" + usr.tcount, JSON.stringify(tnx) );

  swal("Credited : ₹" + parseFloat(sum).toFixed(2) +  "\n\nQuantity : " + q +"\n\nFees (0.15%) : ₹"+ fees, {title: c + " Transaction Successful !",icon: "success"})
  .then((value => { location.reload() } ));
  }
 }
}
loadPrices(); setTimeout(usrWallets, 2000);
