/*
  * Name: Crypto Sim
  * Author: Sai Karthik [kskarthik at disroot dot org]
  * License:GPLv3
*/
'use strict'

const url = "https://koinex.in/api/ticker"

var json, jsons;

// parses API from koinex.in
function loadPrices() {

 fetch(url, {mode: "cors"}).then(function(response) {
    
    return response.json();
    
  }).then(function(myJson) {
  
  JSON.stringify(myJson);
  
  json = myJson.prices.inr; //price inr
  jsons = myJson.stats.inr; // stats

  for (let cn in json) {

  let trow = document.createElement('tr');
  
  trow.innerHTML ="<td class='text-uppercase'>"+ jsons[cn].currency_full_form +"</td>"+"<td>"+ cn +"</td>"+"<td class='inr'>"+ json[cn] +"</td>";
 
    document.querySelector('#table-body').appendChild(trow);
   }
 });
 setTimeout(stats, 1000);
}

loadPrices(); 

// check the current coin price with last 24 low price & show the diff in color
function stats() {

let inr, n; 

inr = document.querySelectorAll('.inr');
n = 0;

 for (let s in jsons) {

  if ( json[s] > jsons[s].min_24hrs ) { 

      inr[n].innerHTML += "<small class='text-success font-weight-light'> " + parseFloat(( json[s] - jsons[s].min_24hrs )).toFixed(2) + " +</small>";
    
    } else {
        
        inr[n].innerHTML += "<small class='text-danger font-weight-light'> " + parseFloat(( json[s] - jsons[s].min_24hrs )).toFixed(2) + " -</small>";  
     } 
  n = n+1;
  }
}
