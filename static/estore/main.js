window.onload = main;
// product list
const item = {
	
	laptop: [
				{
				'model':'mhx230',
				'info':'Refurbished, Privacy respecting laptop with coreboot & open source atheros WiFi chipset, Crippled Intel IME',
				'spec': '4GB RAM / 120GB SSD',
				'img': 'img/mhx.png'
				},

				{
				'model':'mhx230',
				'info':'Refurbished, Privacy respecting laptop with coreboot & open source atheros WiFi chipset, Crippled Intel IME',
				'spec': '8GB RAM / 240GB SSD',
				'img': 'img/mhx.png'
				},
				{
				'model':'mhx230',
				'info':'Refurbished, Privacy respecting laptop with coreboot & open source atheros WiFi chipset, Crippled Intel IME',
				'spec': '16GB RAM / 512GB SSD',
				'img': 'img/mhx.png'
				}
			],

	keyboard: [
				{
				'model': 'keyboard1',
				'info': 'Hackable mechanical keyboard',
				'img': 'img/mhx.png'
				}

			  ]
}
// show the products
function showItems(){

	for (let i in item.laptop) {
		let product = `<div class="col-md-4 text-center">
		<div class='card'>
		<p class='model bg-success text-white card-header text-uppercase'>${item.laptop[i].model}</p>
		<img class='img-fluid card-img-top' src='${item.laptop[i].img}'></img><br>
		<p class='card-text text-black-50'>${item.laptop[i].info}</p>
		<p class='spec font-weight-bold'>${item.laptop[i].spec}</p>
		<div class="order">
			Select Quantity: <select class='quantity'>
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
			</select>
			<br><br>
			</div>
			<div class='card-footer'>
			<button type="button" onclick='new Cart().add(this.parentElement.parentElement.querySelector(".model").textContent, this.parentElement.parentElement.querySelector(".spec").textContent, this.parentElement.parentElement.querySelector("select").value)' class='btn btn-primary btn-sm'>Add To Cart</button>
			</div>
		</div>
		<br>
	</div>`;

	document.querySelector('#laptop').innerHTML += product;

	}
}

// starting point
function main(){

	showItems();
}
// All functions like adding to cart, removing from cart
class Cart {

	add(name, spec, quantity) {

		let duplicateOrder = false;
	
		let date = new Date();
	
		let newOrder = { item: name, spec: spec, qty: quantity, id: date };
	
		if (localStorage.getItem('cart') != null) {
	
			let orders = JSON.parse( localStorage.getItem('cart') );
			
			for ( let n in orders) { // check for duplicate orders
				
				if ( (orders[n].item == newOrder.item) && (orders[n].spec == newOrder.spec) && ( parseInt( newOrder.qty) ) >= ( parseInt( orders[n].qty )) ) {
	
					duplicateOrder = true;
	
					swal('Oops!', 'Item already in cart', 'error');
					
					break;
				}
			}
	
			if (duplicateOrder == false) {
	
				orders.push(newOrder);
	
				localStorage.setItem('cart', JSON.stringify(orders));
	
				swal("Added to cart!",{icon:'success'});
			}
		  }
		  else {
	
			let int = [newOrder];
	
			localStorage.setItem('cart', JSON.stringify(int));
	
			swal("Added to cart !", {icon: 'success'})
	
			}	
	}

	load() {

	const orders = JSON.parse( localStorage.getItem('cart') );

    let cart = document.querySelector('#cart');
    
    cart.innerHTML = '';

    if ( (orders.length == 0) || (orders == null)) {

    	swal('Cart is Empty !',{icon:'info'});
    	new Cart().toggle();
    	return
	}
	for (let i in orders) {

			let product = 
			`<div class='m-1 card col-md-4 text-black container-fluid rounded-sm text-justify'>
			<p class='card-title text-uppercase'><b>Name:</b> ${orders[i].item}</p>
			<p class='text-left'><b>Specs:</b> ${orders[i].spec}</p>
			<p class='spec'><b>Quantity:</b> ${orders[i].qty}</p>
			<div><span class='badge badge-danger' id=${orders[i].id} onclick='new Cart().removeItem(this.id);'>Remove</span></div>
			</div>`;

		cart.innerHTML += product
		}

		cart.innerHTML += `<div class='m-1 container-fluid text-center'><br>
	<a class='btn btn-sm btn-light' href=mailto:abhas@mostlyharmless.io?subject=Order>Place Order</a>
    </div>`
	}
	
	toggle() {

	const c = document.querySelector('#cart');

	if ( c.getAttribute('data-toggle') == 'close' ) {

		c.classList.remove('d-none');
		c.setAttribute('data-toggle', 'open');
		this.load();
		document.querySelector('#list').classList.add('d-none'); // hide products

	}else {

		c.classList.add('d-none');
		c.setAttribute('data-toggle', 'close');
		document.querySelector('#list').classList.remove('d-none'); // show products

		}
 	}

	removeItem(id) {
		const orders = JSON.parse( localStorage.getItem('cart') );

		for (let n in orders) {

			if (id == orders[n].id) {

				orders.splice(n, 1);
				
				localStorage.setItem('cart', JSON.stringify(orders));

				this.load()
				
				break;
			}
		}
	}
}