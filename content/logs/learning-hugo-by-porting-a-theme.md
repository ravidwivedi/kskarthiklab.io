---
title: "Learning Hugo by porting a theme"
date: 2019-11-11T14:31:29+05:30
draft: false
tags: ["hugo"]
keywords: ["hugo"]
---

Since a long time, I was thinking of switching from my old static website to a static site generator. After doing some research, I
settled at hugo. [Hugo](https://gohugo.io) is a popular static site generator created in golang.

The reasons i chose hugo are:

* _It is a single binary to download & run_
* _It is super fast_ ( builds my entire site in ~10ms) :smiley:
* _More control over the appearance of website & content_
* _SEO friendly websites_
* _Most of the required features are available built-in_

I was very pleased with hugo & decided to contribute back something to the amazing hugo community. I chose to port a theme. The Reason is that it serves a dual purpose, I can learn in-depth about hugo & in return i will have a theme to contribute.

So, I picked up an existing CC-BY-3.0 licensed HTML5 restaurant business theme named [Resto](https://freehtml5.co/preview/?item=resto-free-responsive-bootstrap-4-template-for-restaurants) created by [freehtml5.co](https://freehtml5.co). Hugo has an amazing, well maintained documentation to get started. But, Initially i was a bit confused about using it. Later, I came across Mike Dane's [Hugo Tutorial playlist](https://invidio.us/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) for beginners. He explained about the basic usage & terminologies very well. I would recommend watching this for new users & then head over to documentation to learn more in-depth.

Finally, After a few days of learning, I have successfully completed porting my first theme & named it as **Resto Hugo** ([Source code](https://gitlab.com/kskarthik/resto-hugo)). I have submitted it to the [hugo themes showcase](https://themes.gohugo.io/resto-hugo/) & it was approved !

#### *Conclusion*

Learning hugo was fun & exciting. For beginners, There's a bit of steep learning curve involved. But once you get familiar, you will start liking it!  
