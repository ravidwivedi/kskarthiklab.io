---
title: "About"
date: 2019-04-19T21:37:58+05:30
type: "page"
---

I'm Karthik, I'm interested in open source & web technologies.

#### __MY PROJECTS__
[GNUKhata-Docker](https://gitlab.com/kskarthik/gnukhata-docker) - Docker Images for GNUKhata

[aerc-docker](https://gitlab.com/kskarthik/aerc-docker) - aerc email client docker image (just 30~ MB)

[YTDL](https://gitlab.com/kskarthik/ytdl) - A simple command line python tool to download youtube audio

[CryptoSim](https://kskarthik.gitlab.io/cryptosim) - A crypto currency trading game using koinex API

[eStore](https://kskarthik.gitlab.io/estore) - Simple page ecommerce site with cart functionality using javascript, bootstrap

[Sailfish OS for Mozilla Flame](https://together.jolla.com/question/155313/alpha-sailfishos-v2056-rom-for-mozilla-flame/)

[postmarketOS for Sony Z3C](https://wiki.postmarketos.org/wiki/Sony_Xperia_Z3C_(sony-aries))

##### HUGO THEMES

[monopriv](https://themes.gohugo.io/monopriv/): A lightweight bootstrap blogging theme

[resto-hugo](https://themes.gohugo.io/resto-hugo/): Port of resto theme to hugo

##### FIREFOX ADD-ON'S

[ProtonPlus](https://addons.mozilla.org/en-US/firefox/addon/protonplus/) - Unlocks a few premium features for free protonmail account

[Indic Input](https://addons.mozilla.org/en-US/firefox/addon/indic-input/") - Enables the user to write in serveral Indian languages directly on webpages